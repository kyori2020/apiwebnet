﻿using System;
using System.Collections.Generic;

namespace CoWork.Data.CoworkModels;

public partial class TblVisitum
{
    public string CodigoVisita { get; set; } = null!;

    public string TipoVisita { get; set; } = null!;

    public DateTime? FechayHoraVisita { get; set; }

    public int Usuario { get; set; }

    public virtual TblUsuario UsuarioNavigation { get; set; } = null!;
}
