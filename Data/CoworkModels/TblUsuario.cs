﻿using System;
using System.Collections.Generic;

namespace CoWork.Data.CoworkModels;

public partial class TblUsuario
{
    public int CodigoUsuario { get; set; }

    public string Nombre { get; set; } = null!;

    public string Apellidos { get; set; } = null!;

    public string Correo { get; set; } = null!;

    public string TelefonoMovil { get; set; } = null!;

    public string TipoDoc { get; set; } = null!;

    public string Membresia { get; set; } = null!;

    public virtual TblMembresium MembresiaNavigation { get; set; } = null!;

    public virtual ICollection<TblAlquiler> TblAlquilers { get; set; } = new List<TblAlquiler>();

    public virtual ICollection<TblComprobante> TblComprobantes { get; set; } = new List<TblComprobante>();

    public virtual ICollection<TblVisitum> TblVisita { get; set; } = new List<TblVisitum>();

    public virtual TblTipoDoc TipoDocNavigation { get; set; } = null!;
}
