﻿using System;
using System.Collections.Generic;

namespace CoWork.Data.CoworkModels;

public partial class TblAlquiler
{
    public int CodigoAlquiler { get; set; }

    public string TiempoAlquiler { get; set; } = null!;

    public DateTime FechayHoraInicio { get; set; }

    public DateTime FechayHoraFinalizado { get; set; }

    public int Usuario { get; set; }

    public string Servicios { get; set; } = null!;

    public int Comprobante { get; set; }

    public virtual TblComprobante ComprobanteNavigation { get; set; } = null!;

    public virtual TblServicio ServiciosNavigation { get; set; } = null!;

    public virtual TblUsuario UsuarioNavigation { get; set; } = null!;
}
