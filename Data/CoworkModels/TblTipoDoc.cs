﻿using System;
using System.Collections.Generic;

namespace CoWork.Data.CoworkModels;

public partial class TblTipoDoc
{
    public string CodigoTipoDoc { get; set; } = null!;

    public string NombreTipoDoc { get; set; } = null!;

    public virtual ICollection<TblUsuario> TblUsuarios { get; set; } = new List<TblUsuario>();
}
