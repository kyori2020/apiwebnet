﻿using System;
using System.Collections.Generic;

namespace CoWork.Data.CoworkModels;

public partial class TblPlane
{
    public string CodigoPlanes { get; set; } = null!;

    public string TipoPlan { get; set; } = null!;

    public decimal PrecioPlan { get; set; }

    public virtual ICollection<TblServicio> Servicios { get; set; } = new List<TblServicio>();
}
