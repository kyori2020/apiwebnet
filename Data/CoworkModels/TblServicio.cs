﻿using System;
using System.Collections.Generic;

namespace CoWork.Data.CoworkModels;

public partial class TblServicio
{
    public string CodigoServicios { get; set; } = null!;

    public string TipoServicio { get; set; } = null!;

    public virtual ICollection<TblAlquiler> TblAlquilers { get; set; } = new List<TblAlquiler>();

    public virtual ICollection<TblComprobante> TblComprobantes { get; set; } = new List<TblComprobante>();

    public virtual ICollection<TblPlane> Planes { get; set; } = new List<TblPlane>();
}
