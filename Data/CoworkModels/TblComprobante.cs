﻿using System;
using System.Collections.Generic;

namespace CoWork.Data.CoworkModels;

public partial class TblComprobante
{
    public int CodigoComprobante { get; set; }

    public string TipoComprobante { get; set; } = null!;

    public DateTime FechayHoraComprobante { get; set; }

    public int Usuario { get; set; }

    public string Servicio { get; set; } = null!;

    public string MetodoPago { get; set; } = null!;

    public virtual TblMetodoPago MetodoPagoNavigation { get; set; } = null!;

    public virtual TblServicio ServicioNavigation { get; set; } = null!;

    public virtual ICollection<TblAlquiler> TblAlquilers { get; set; } = new List<TblAlquiler>();

    public virtual TblUsuario UsuarioNavigation { get; set; } = null!;
}
