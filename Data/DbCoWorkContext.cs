﻿using System;
using System.Collections.Generic;
using CoWork.Data.CoworkModels;
using Microsoft.EntityFrameworkCore;

namespace CoWork.Data;

public partial class DbCoWorkContext : DbContext
{
    public DbCoWorkContext()
    {
    }

    public DbCoWorkContext(DbContextOptions<DbCoWorkContext> options)
        : base(options)
    {
    }

    public virtual DbSet<TblAlquiler> TblAlquilers { get; set; }

    public virtual DbSet<TblComprobante> TblComprobantes { get; set; }

    public virtual DbSet<TblMembresium> TblMembresia { get; set; }

    public virtual DbSet<TblMetodoPago> TblMetodoPagos { get; set; }

    public virtual DbSet<TblPlane> TblPlanes { get; set; }

    public virtual DbSet<TblServicio> TblServicios { get; set; }

    public virtual DbSet<TblTipoDoc> TblTipoDocs { get; set; }

    public virtual DbSet<TblUsuario> TblUsuarios { get; set; }

    public virtual DbSet<TblVisitum> TblVisita { get; set; }



    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TblAlquiler>(entity =>
        {
            entity.HasKey(e => e.CodigoAlquiler).HasName("PK__tblAlqui__AC48441FC5122D48");

            entity.ToTable("tblAlquiler");

            entity.Property(e => e.CodigoAlquiler).ValueGeneratedNever();
            entity.Property(e => e.FechayHoraFinalizado).HasColumnType("datetime");
            entity.Property(e => e.FechayHoraInicio).HasColumnType("datetime");
            entity.Property(e => e.Servicios)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.TiempoAlquiler)
                .HasMaxLength(30)
                .IsUnicode(false);

            entity.HasOne(d => d.ComprobanteNavigation).WithMany(p => p.TblAlquilers)
                .HasForeignKey(d => d.Comprobante)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__tblAlquil__Compr__3C69FB99");

            entity.HasOne(d => d.ServiciosNavigation).WithMany(p => p.TblAlquilers)
                .HasForeignKey(d => d.Servicios)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__tblAlquil__Servi__3B75D760");

            entity.HasOne(d => d.UsuarioNavigation).WithMany(p => p.TblAlquilers)
                .HasForeignKey(d => d.Usuario)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__tblAlquil__Usuar__3A81B327");
        });

        modelBuilder.Entity<TblComprobante>(entity =>
        {
            entity.HasKey(e => e.CodigoComprobante).HasName("PK__tblCompr__47C6A8DEE99D5AC4");

            entity.ToTable("tblComprobante");

            entity.Property(e => e.CodigoComprobante).ValueGeneratedNever();
            entity.Property(e => e.FechayHoraComprobante).HasColumnType("datetime");
            entity.Property(e => e.MetodoPago)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Servicio)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.TipoComprobante)
                .HasMaxLength(30)
                .IsUnicode(false);

            entity.HasOne(d => d.MetodoPagoNavigation).WithMany(p => p.TblComprobantes)
                .HasForeignKey(d => d.MetodoPago)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__tblCompro__Metod__37A5467C");

            entity.HasOne(d => d.ServicioNavigation).WithMany(p => p.TblComprobantes)
                .HasForeignKey(d => d.Servicio)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__tblCompro__Servi__36B12243");

            entity.HasOne(d => d.UsuarioNavigation).WithMany(p => p.TblComprobantes)
                .HasForeignKey(d => d.Usuario)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__tblCompro__Usuar__35BCFE0A");
        });

        modelBuilder.Entity<TblMembresium>(entity =>
        {
            entity.HasKey(e => e.CodigoMembresia).HasName("PK__tblMembr__2494F508DD803C4B");

            entity.ToTable("tblMembresia");

            entity.Property(e => e.CodigoMembresia)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.TipoMembresia)
                .HasMaxLength(23)
                .IsUnicode(false);
        });

        modelBuilder.Entity<TblMetodoPago>(entity =>
        {
            entity.HasKey(e => e.CodigoMetodoPago).HasName("PK__tblMetod__6F0B3995B807B022");

            entity.ToTable("tblMetodoPago");

            entity.Property(e => e.CodigoMetodoPago)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.DetalledePago)
                .HasMaxLength(60)
                .IsUnicode(false);
            entity.Property(e => e.TipodePago)
                .HasMaxLength(30)
                .IsUnicode(false);
        });

        modelBuilder.Entity<TblPlane>(entity =>
        {
            entity.HasKey(e => e.CodigoPlanes).HasName("PK__tblPlane__E8771123700D71A4");

            entity.ToTable("tblPlanes");

            entity.Property(e => e.CodigoPlanes)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.PrecioPlan).HasColumnType("smallmoney");
            entity.Property(e => e.TipoPlan)
                .HasMaxLength(11)
                .IsUnicode(false);

            entity.HasMany(d => d.Servicios).WithMany(p => p.Planes)
                .UsingEntity<Dictionary<string, object>>(
                    "TblPlanesServicio",
                    r => r.HasOne<TblServicio>().WithMany()
                        .HasForeignKey("Servicios")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK__tblPlanes__Servi__32E0915F"),
                    l => l.HasOne<TblPlane>().WithMany()
                        .HasForeignKey("Planes")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK__tblPlanes__Plane__31EC6D26"),
                    j =>
                    {
                        j.HasKey("Planes", "Servicios").HasName("PK__tblPlane__496426CE94B3E938");
                        j.ToTable("tblPlanesServicios");
                        j.IndexerProperty<string>("Planes")
                            .HasMaxLength(7)
                            .IsUnicode(false)
                            .IsFixedLength();
                        j.IndexerProperty<string>("Servicios")
                            .HasMaxLength(7)
                            .IsUnicode(false)
                            .IsFixedLength();
                    });
        });

        modelBuilder.Entity<TblServicio>(entity =>
        {
            entity.HasKey(e => e.CodigoServicios).HasName("PK__tblServi__6F317A5D6DBA2C75");

            entity.ToTable("tblServicios");

            entity.Property(e => e.CodigoServicios)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.TipoServicio)
                .HasMaxLength(23)
                .IsUnicode(false);
        });

        modelBuilder.Entity<TblTipoDoc>(entity =>
        {
            entity.HasKey(e => e.CodigoTipoDoc).HasName("PK__tblTipoD__A4FE981A62163516");

            entity.ToTable("tblTipoDoc");

            entity.Property(e => e.CodigoTipoDoc)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.NombreTipoDoc)
                .HasMaxLength(3)
                .IsUnicode(false);
        });

        modelBuilder.Entity<TblUsuario>(entity =>
        {
            entity.HasKey(e => e.CodigoUsuario).HasName("PK__tblUsuar__F0C18F580622A0DB");

            entity.ToTable("tblUsuario");

            entity.Property(e => e.CodigoUsuario).ValueGeneratedNever();
            entity.Property(e => e.Apellidos)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Correo)
                .HasMaxLength(40)
                .IsUnicode(false);
            entity.Property(e => e.Membresia)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.TelefonoMovil)
                .HasMaxLength(9)
                .IsUnicode(false)
                .HasColumnName("Telefono_Movil");
            entity.Property(e => e.TipoDoc)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();

            entity.HasOne(d => d.MembresiaNavigation).WithMany(p => p.TblUsuarios)
                .HasForeignKey(d => d.Membresia)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__tblUsuari__Membr__2F10007B");

            entity.HasOne(d => d.TipoDocNavigation).WithMany(p => p.TblUsuarios)
                .HasForeignKey(d => d.TipoDoc)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__tblUsuari__TipoD__2E1BDC42");
        });

        modelBuilder.Entity<TblVisitum>(entity =>
        {
            entity.HasKey(e => e.CodigoVisita).HasName("PK__tblVisit__0715B1E937A92485");

            entity.ToTable("tblVisita");

            entity.Property(e => e.CodigoVisita)
                .HasMaxLength(7)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.FechayHoraVisita).HasColumnType("datetime");
            entity.Property(e => e.TipoVisita)
                .HasMaxLength(20)
                .IsUnicode(false);

            entity.HasOne(d => d.UsuarioNavigation).WithMany(p => p.TblVisita)
                .HasForeignKey(d => d.Usuario)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__tblVisita__Usuar__3F466844");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
