using Microsoft.AspNetCore.Mvc;
using CoWork.Data;
using CoWork.Data.CoworkModels;

namespace CoWork.Controllers;

[ApiController]
[Route("[controller]")]
public class ServicioController: ControllerBase{
    private readonly DbCoWorkContext _context;
    public ServicioController(DbCoWorkContext context){
        _context=context;



    }
    [HttpGet]
    public IEnumerable<TblServicio>Get(){
        return _context.TblServicios.ToList();
    }
    [HttpGet("{id}")]
    public ActionResult<TblServicio>GetById(int id){
        var servicio =_context.TblServicios.Find(id);

        if (servicio is null){
            return NotFound();
        }
        return servicio;
    }

}