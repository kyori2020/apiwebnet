﻿using System;
using System.Collections.Generic;

namespace CoWork.Data.CoworkModels;

public partial class TblMetodoPago
{
    public string CodigoMetodoPago { get; set; } = null!;

    public string TipodePago { get; set; } = null!;

    public string DetalledePago { get; set; } = null!;

    public virtual ICollection<TblComprobante> TblComprobantes { get; set; } = new List<TblComprobante>();
}
